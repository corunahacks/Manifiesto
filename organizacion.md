Organización interna de los Coruña Hacks
========================================

1. Se realizará el primer sábado de cada mes.

2. Para que un proyecto entre dentro de los eventos Coruña Hacks deberá contar con al menos 4 personas interesadas en participar en su creación y desarrollo.

3. En cada sesión de Coruña Hacks habrá la posiblidad de que 1 nuevo proyecto pueda entrar.

4. En cada sesión con el fin de integrar y poner en contexto a la gente nueva que se acerque al evento, se reservará unos minutos al principio y al final de cada sesión. 

  - Al principio de la sesión se presentarán los proyectos en los que se están trabajando y la situación actual.
  - Al final de la sesión (en 1 o 2 minutos) se contarán los avances y situación actual del proyecto para ver cómo han avanzado y en qué estado quedan hasta la siguiente sesión.

5. El horario será de 10 a 14 y de 15 a 20.
