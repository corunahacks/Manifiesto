Manifiesto Coruña Hacks
=======================

1. El objetivo final de Coruña Hacks es unir a la comunidad desarrolladora de manera periódica para colaborar en proyectos de software libre que puedan tener una utilidad real.

2. Se trata de un evento independiente, y su organización interna se regirá por principios democráticos.

3. Si hubiese patrocinios externos, éstos no podrán influir en el funcionamiento habitual y las formas de actuar propias de la comunidad de Coruña Hacks.

4. El centro de Coruña Hacks es el código y proyecto que la comunidad desarrolle, y todo irá organizado en torno a él.

5. Se realizará el primer sábado de cada mes.

5. Todos los proyectos que se realizan en los Coruña Hacks deben ser software o hardware libre.

6. Para colaborar no es necesario acudir físicamente a las sesiones, el código estará publicado de manera pública y cualquier persona desde cualquier lugar podrá usarlo y colaborar.

7. Todo el código que se genere con el fin de que sea accesible, fácil para realizar un seguimiento y poder colaborar gente nueva ya sea en los eventos físicos o de manera remota, estará en gitlab dentro de la organización [corunahacks][gitlab].

8. Cualquier persona puede cambiar de proyecto en cualquier momento y por cualquier motivo.

9. En cada sesión de Coruña Hacks habrá la posiblidad de que 1 nuevo proyecto pueda entrar.

10. Se respetará el [documento de organización][ri] y el [código de conducta][cc] de la comunidad.

  [gitlab]: https://gitlab.com/corunahacks
  [ri]: https://gitlab.com/corunahacks/Manifiesto/blob/master/organizacion.md
  [cc]: https://gitlab.com/corunahacks/Manifiesto/blob/master/code-of-conduct.md
